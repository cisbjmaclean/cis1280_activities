/*
BJM
20160223
This shows the number of ingredients element if the pizza checkbox is checked.
*/

function clickPizza(cb) {
  console.log("Clicked, new value = " + cb.checked);
  if(cb.checked){
	  document.getElementById("pizzaIngredientNumber").style.display = "inline";
  } else {
	  document.getElementById("pizzaIngredientNumber").style.display = "none";
  }   
}

/*
BJM 
20160223
This method will do all of the processing associated with making the order.
*/

function submitPizzaOrder(){
var total = 0;
var details = "";

//Validate the required fields.
var firstName = document.getElementById("firstName").value;
var lastName = document.getElementById("lastName").value;
var phoneNumber = document.getElementById("phoneNumber").value;
var address = document.getElementById("address").value;

console.log("address="+address);

/*
For the delivery type we have to check to see which of the elements (deliveryType radio buttons) is checked.  The getRadioVal function will 
loop through the elements and return the value of the one that is checked.
*/

var deliveryType = getRadioVal(document.getElementsByName("deliveryType"));
console.log("deliveryType="+deliveryType);

/*
If the user chose homeDelivery - then we have to treat the address as required.  To do this we will set the class of the address to required.  
It will then be treated as such below when we validate required elements.
*/

if(deliveryType == "homeDelivery" && address == ""){
  console.log("Need an address but one was not provided.");
  //set the class of the address to required!
  document.getElementById("address").className = "required";
}else{
  //reset the address element's background to white.	
  document.getElementById("address").style.backgroundColor = "white";
}

/*
Validate the required fields.  Check the array of required elements 
to make sure that they have a value
*/

var validRequiredFields = true;
var requiredElements = document.getElementsByClassName("required");
var i;
console.log("There are "+requiredElements.length+" required elements");

//loop through each required element and make sure a value is provided.
for (i = 0; i < requiredElements.length; i++) {
	var theElement = requiredElements[i];
    theElement.style.backgroundColor = "lightyellow";

	//If a value is not provided handle it as invalid.  
	if(theElement.value == ""){
	  validRequiredFields = false;  //flag to indicate not valid
      console.log("found an empty required element-"+theElement.id);
      theElement.style.backgroundColor = "red";
	}
}



/* Create the details for the receipt */
var name="Name: "+firstName+" "+lastName;
var phone="Phone: "+phoneNumber;
var address= "Address: \n"+document.getElementById("address").value;

//calculate total cost.
var pizza = "";
if(document.getElementById("wantPizza").checked){
	total += 15;
	//Add $1 for each ingredient
	var ingredientsDetail = parseInt(document.getElementById("quantityOfIngredients").value);
	total += (ingredientsDetail*1);
	pizza = "Pizza ("+ingredientsDetail+" ingredients) $"+total;
}

//Pasta details
var pasta = "";
if(document.getElementById("wantPasta").checked){
	total += 12;
	pasta="Pasta $"+12;
}

//Breadstick details
var breadsticks = "";
if(document.getElementById("wantBreadSticks").checked){
	total += 8;
	breadsticks= "Break Sticks $ $"+8;
}

/*
Handle validation issues.  There can be validation issues associated 
with not providing required field values and also with not selecting
anything on the order.
*/

var validOrder = (total > 0);
var errorMessage = "";

if(!validOrder){
		errorMessage += "You must choose something from the menu to make an order!\n";
} 
if(!validRequiredFields){
		errorMessage += "You must complete the required fields (see red)!\n";
}

//If validation issues, give an error alert otherwise continue processing.
if(!(validOrder && validRequiredFields)){
	alert(errorMessage);
}
else {  //Continue

  var delivery = "";
  if(document.getElementById("deliveryType").value == "homeDelivery"){
	total += 5;
	delivery= "Delivery $"+5;
  }

  var total= "Total $"+total+"\n";

  //Create formatted details (use \n for line feed in alert statements.)
  var line="\n";
  details = name+line+phone+line+address+line+line+pizza+line+pasta+line+breadsticks+line+"---------------"+line+delivery+line+total+line;


  var confirmed = window.confirm(details+line+"Are you sure you want to make this order?");

  /*
  Want to format the email nicely.  Use different line feed characters.  See: http://stackoverflow.com/questions/15019689/html-insert-line-break-in-email-subject-like-20-is-a-space
  */

  line="%0D%0A";
  details = name+line+phone+line+address+line+line+pizza+line+pasta+line+breadsticks+line+"---------------"+line+delivery+line+total+line;

  /*
  If confirmed then send an email with the details.
  */
  if(confirmed == true){
    document.getElementById("orderButton").href="mailto:youTEST@yourdomain.com?subject=Big Jim Receipt&body="+details;
  } else {
	document.getElementById("orderButton").href="#";
  }
}

}

/*
BJM from http://www.dyn-web.com/tutorials/forms/radio/get-selected.php
20160223
*/
function getRadioVal(radios) {
    var val;
    
    // loop through list of radio buttons
    for (var i=0, len=radios.length; i<len; i++) {
        if ( radios[i].checked ) { // radio checked?
            val = radios[i].value; // if so, hold its value in val
            break; // and break out of for loop
        }
    }
    return val; // return value of checked radio or undefined if none checked
}